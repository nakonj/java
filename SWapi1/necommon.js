window.addEventListener('load', init, false);
function init() {
    var btnAllPerson = document.querySelectorAll(".button-start")[0];
    btnAllPerson.addEventListener('click', getApi, false);
    var next = document.querySelectorAll('.next-batton')[0];
    next.addEventListener('click', function () {
        url = Base.next;
        Request(url, showStarwars);
    })
}
function getApi() {
    var urlFilms = "https://swapi.co/api/people/";
    Request(urlFilms, showStarwars);
}

function Request(urlFilms, callback) {
    var xhr = new XMLHttpRequest();
    xhr.open("GET", urlFilms, true);
    xhr.onreadystatechange = function () {
        if (xhr.readyState == 4) {
            if (xhr.status == 200) {
                var data = xhr.responseText;
                data = JSON.parse(data);
                console.log(data);
                Base.next = data.next;
                hideLoader();
                callback(data);
            }
        }
    };
    xhr.send();
    showLoader();
}
function showLoader (){
    document.getElementsByClassName("loader")[0].style.display = "block";
    // document.getElementsByClassName("button-start")[0].style.display = "none";
}
function hideLoader (){
    document.getElementsByClassName("loader")[0].style.display = "none";
}
var Base = {
    next: null,
    pagination: function () {
        var pagination = document.querySelectorAll('.button-none')[0];
        pagination.style.display = "block";
    }
};
var showPagination = this.getElmentbyClass('inbox-pagination');
showPagination.style.display = "block";
var personCount = this.getElementbyClass('inbox-pagination');
var count = personCount.getElementsByName('span')[0];
if (Base.nextPage != null){
    count.innerText = currentPage[1] - 1 + "0" + Base.count;
}

function showStarwars(data) {
    var main = document.getElementsByClassName("main")[0];
    var table = document.createElement('table');
    main.appendChild(table);
    var thead = document.createElement('thead');
    table.appendChild(thead);
    var trH = document.createElement("tr");
    thead.appendChild(trH);
    var thH = document.createElement("th");
    trH.appendChild(thH);
    thH.innerHTML = ('Name');
    var thH = document.createElement("th");
    trH.appendChild(thH);
    thH.innerHTML = ('height');
    var thH = document.createElement("th");
    trH.appendChild(thH);
    thH.innerHTML = ('Mass');
    var thH = document.createElement("th");
    trH.appendChild(thH);
    thH.innerHTML = ('skin_color');
    var thH = document.createElement("th");
    trH.appendChild(thH);
    thH.innerHTML = ('birth_year');
    var thH = document.createElement("th");
    trH.appendChild(thH);
    thH.innerHTML = ('eye_color');
    var thH = document.createElement("th");
    trH.appendChild(thH);
    thH.innerHTML = ('gender');
    var thH = document.createElement("th");
    trH.appendChild(thH);
    thH.innerHTML = ('hair_color');
    Base.pagination();
    var tbody = document.createElement('tbody');
    table.appendChild(tbody);
    for (var i = 0; i < data.results.length; i++){
        console.log(data.results[i]);
        var tr = document.createElement('tr');
        tbody.insertBefore(tr, tbody.nextSibling);
        var name = document.createElement('td');
        name.innerHTML = data.results[i].name;
        tr.insertBefore(name, tr.nextSibling);
        var height = document.createElement('td');
        height.innerHTML = data.results[i].height;
        tr.insertBefore(height, tr.nextSibling);
        var mass = document.createElement('td');
        mass.innerHTML = data.results[i].mass;
        tr.insertBefore(mass, tr.nextSibling);
        var skin_color = document.createElement('td');
        skin_color.innerHTML = data.results[i].skin_color;
        tr.insertBefore(skin_color, tr.nextSibling);
        var birth_year = document.createElement('td');
        birth_year.innerHTML = data.results[i].birth_year;
        tr.insertBefore(birth_year, tr.nextSibling);
        var eye_color = document.createElement('td');
        eye_color.innerHTML = data.results[i].eye_color;
        tr.insertBefore(eye_color, tr.nextSibling);
        var gender = document.createElement('td');
        gender.innerHTML = data.results[i].gender;
        tr.insertBefore(gender, tr.nextSibling);
        var hair_color = document.createElement('td');
        hair_color.innerHTML = data.results[i].hair_color;
        tr.insertBefore(hair_color, tr.nextSibling);

    }
}

var root = 'https://swapi.co/api/starships';
function ajaxrequest(data) {
    var starships = data.results;
    for (i = 0; i < starships.length; i++) {
        var nameList = "<li class='starships'>" + starships[i].name +
            "<ul class='starship-class'><li>Model: " + starships[i].model + "</li></ul>" +
            "</li>";
        $('.swapi').append(nameList);
    }
};
$.ajax({
    url: root,
    method: 'GET'
}).then(ajaxrequest);



