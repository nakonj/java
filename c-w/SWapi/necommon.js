
function start() {
    var urlFilms = "https://swapi.co/api/people/";
    Request(urlFilms, showStarwars);
}


function Request(url, callback) {

    var xhr = new XMLHttpRequest();

    xhr.open("GET", url, true);
    xhr.onreadystatechange = function () {
        if (xhr.readyState == 4) {
            if (xhr.status == 200) {
                var data = xhr.responseText;
                data = JSON.parse(data);
                callback(data);
                Base.unloader();
                Base.unbutton();
                Base.buttons();
            }
        }
    };
    xhr.send();
    Base.loader();
}



var Base = {

    loader: function() {
        document.getElementsByClassName("loader")[0].style.display = 'block';
    },

    unloader : function() {
        document.getElementsByClassName("loader")[0].style.display = 'none';
    },

    unbutton : function() {
        document.getElementsByClassName("btnClick")[0].style.display = 'none';
    },

    buttons : function() {
        document.getElementsByClassName("next-back")[0].style.display = 'block';
        document.getElementsByClassName("next-back")[1].style.display = 'block';
    }

};



function showStarwars(data) {

    var main = document.getElementsByClassName("main")[0];
    var table = document.createElement('table');
    main.appendChild(table);
    var thead = document.createElement('thead');
    table.appendChild(thead);
    var trH = document.createElement("tr");
    thead.appendChild(trH);

    var thH = document.createElement("th");
    trH.appendChild(thH);
    thH.innerHTML = ('Name');
    var thH = document.createElement("th");
    trH.appendChild(thH);
    thH.innerHTML = ('height');
    var thH = document.createElement("th");
    trH.appendChild(thH);
    thH.innerHTML = ('Mass');
    var thH = document.createElement("th");
    trH.appendChild(thH);
    thH.innerHTML = ('skin_color');
    var thH = document.createElement("th");
    trH.appendChild(thH);
    thH.innerHTML = ('birth_year');
    var thH = document.createElement("th");
    trH.appendChild(thH);
    thH.innerHTML = ('eye_color');
    var thH = document.createElement("th");
    trH.appendChild(thH);
    thH.innerHTML = ('gender');
    var thH = document.createElement("th");
    trH.appendChild(thH);
    thH.innerHTML = ('hair_color');

    var tbody = document.createElement('tbody');
    table.appendChild(tbody);
    for (var i = 0; i < data.results.length; i++){
        console.log(data.results[i]);
        var tr = document.createElement('tr');
        tbody.insertBefore(tr, tbody.nextSibling);

        var name = document.createElement('td');
        name.innerHTML = data.results[i].name;
        tr.insertBefore(name, tr.nextSibling);

        var height = document.createElement('td');
        height.innerHTML = data.results[i].height;
        tr.insertBefore(height, tr.nextSibling);

        var mass = document.createElement('td');
        mass.innerHTML = data.results[i].mass;
        tr.insertBefore(mass, tr.nextSibling);

        var skin_color = document.createElement('td');
        skin_color.innerHTML = data.results[i].skin_color;
        tr.insertBefore(skin_color, tr.nextSibling);

        var birth_year = document.createElement('td');
        birth_year.innerHTML = data.results[i].birth_year;
        tr.insertBefore(birth_year, tr.nextSibling);

        var eye_color = document.createElement('td');
        eye_color.innerHTML = data.results[i].eye_color;
        tr.insertBefore(eye_color, tr.nextSibling);

        var gender = document.createElement('td');
        gender.innerHTML = data.results[i].gender;
        tr.insertBefore(gender, tr.nextSibling);

        var hair_color = document.createElement('td');
        hair_color.innerHTML = data.results[i].hair_color;
        tr.insertBefore(hair_color, tr.nextSibling);

    }

}





