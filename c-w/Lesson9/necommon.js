
window.onload = function () {
    var url = "https://www.cryptopia.co.nz/api/GetCurrencies";
    Request(url, ShowCurrency);

};


function Request(url, callback) {

    var xhr = new XMLHttpRequest();

    xhr.open("GET", url, true);
    xhr.onreadystatechange = function () {
        if (xhr.readyState == 4) {
            if (xhr.status == 200) {
                var data = xhr.responseText;
                data = JSON.parse(data);
                console.log(data);
                callback(data);

            }
        }
    };
    xhr.send();
}

function ShowCurrency(data) {
    var main = document.getElementsByClassName("main")[0];
    var table = document.createElement('table');
    main.appendChild(table);
    var thead = document.createElement('thead');
    table.appendChild(thead);
    var tbody = document.createElement('tbody');
    table.appendChild(tbody);
    for (var i = 0; i < data.Data.length; i++){
        console.log(data.Data[i]);
        var tr = document.createElement('tr');
        tbody.insertBefore(tr, tbody.nextSibling);
        var name = document.createElement('td');
        name.innerHTML = data.Data[i].Name;
        tr.insertBefore(name, tr.nextSibling);

    }

}



